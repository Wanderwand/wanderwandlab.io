const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");

module.exports = {
	entry: "./src/index.js",
	output: {
		path: path.resolve(__dirname, "public"),
		filename: "bundle.js"
	},
	module: {
		rules: [
			{
				test: /\.s?css$/i,
				use: [
					"style-loader",
					{ loader: "css-loader", options: { importLoaders: 1 } },
					"postcss-loader",
					"sass-loader"
				]
			},
			{
				test: /\.pug$/,
				use: ["pug-loader"]
			},
	
			{
				test: /\.m?js$/,
				exclude: /(node_modules|bower_components)/,
				use: {
					loader: "babel-loader",
					options: {
						presets: ["@babel/preset-env"]
					}
				}
			}
		]
	},
	plugins: [
		new HtmlWebpackPlugin({
			template: "./src/index.pug"
		}),
		new CopyWebpackPlugin([
			{
				from: "./src/assets/img/*.*",
				to: "./assets/img/[name].[ext]"
			}
			// { from: 'other', to: 'public' },
		])
	],
	devServer: {
		// contentBase: path.join(__dirname, 'public'),
		contentBase: ["./src", "./public"],
		compress: true,
		port: 3000
	}
};
